(** All the try-outs follow the same structure. This specific try-out is teh
    simplest of them all: it has the structure but nothing to it. *)

(** One of the aims of the try-outs is to show the flow of control of the
    program. We simply print to stdout to do so. *)
let traceln s = print_endline s

type _ Effect.t +=
  | Outer : int -> unit Lwt.t Effect.t
    (** [Outer] is an effect with a handler set up outside of the call to
        [Lwt_main.run]. The [int] argument is to indicate how many
        [Lwt.pause ()] should be used in the handler.  *)
  | Inner : int -> unit Lwt.t Effect.t
    (** [Inner] is an effect with a handler set up inside of the call to
        [Lwt_main.run]. The [int] argument is to indicate how many
        [Lwt.pause ()] should be used in the handler.  *)

let rec pause_n n =
  traceln (Printf.sprintf "Pausing %d times" n);
  let open Lwt.Syntax in
  if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n (n - 1)

(** In this first try-out, both of the handlers simply return a promise. The
    promise is already resolved if the effect carries [0] as a payload,
    otherwise it is pending. *)

let outer_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Outer n -> Some (fun k ->
        traceln "Handling Outer effect";
        Effect.Deep.continue k (pause_n n)
    )
    | _ -> None

let inner_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Inner n -> Some (fun k ->
        traceln "Handling Inner effect";
        Effect.Deep.continue k (pause_n n)
    )
    | _ -> None

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        Effect.Deep.try_with
          (fun () ->
            traceln "Entering Inner handler";
            traceln "Perfoming Outer(0) effect";
            let* () = Effect.perform (Outer 0) in
            traceln "Perfomed Outer(0) effect";
            traceln "Perfoming Inner(0) effect";
            let* () = Effect.perform (Inner 0) in
            traceln "Perfomed Inner(0) effect";
            traceln "Exiting Inner handler";
            Lwt.return ()
          ) ()
          { effc = inner_eff }
      );
      traceln "Exited Inner handler";
      traceln "Exiting Outer handler";
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"

