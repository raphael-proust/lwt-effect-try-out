(** In this try-out we wakeup (i.e., resolve) a promise from within a handler. *)

let traceln s = print_endline s

type _ Effect.t +=
  | Outer : unit Lwt.u -> unit Effect.t

let rec pause_n src n =
  traceln (Printf.sprintf "Pausing(%s) %d times" src n);
  let open Lwt.Syntax in
  if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n src (n - 1)

let outer_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Outer u -> Some (fun k ->
        Lwt.wakeup u ();
        Effect.Deep.continue k ()
    )
    | _ -> None

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        let (p, u) = Lwt.wait () in
        let pp =
          let* () = Lwt.pause () in
          traceln "Waiting for wakeup";
          let* () = p in
          traceln "Woken up";
          let* () = Lwt.pause () in
          Lwt.return ()
        in
        let* () = pause_n "stalling" 5 in
        traceln "Perfoming Outer effect";
        let () = Effect.perform (Outer u) in
        traceln "Perfomed Outer effect";
        let* () = pp in
        traceln "Done";
        Lwt.return ()
      );
      traceln "Exiting Outer handler"
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"

(** Note that "Woken up" is printed before "Performed Outer effect" because of
    Lwt's eagerness: the waking up causes immediate calls to the attached
    callbacks. *)
