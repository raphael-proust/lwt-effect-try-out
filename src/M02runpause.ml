(** In this try-out we use [Lwt_main.run] to wrap the problematic pauses in the
    handler. This fails because [Lwt_main.run] cannot be nested. *)

let traceln s = print_endline s

type _ Effect.t +=
  | Outer : int -> unit Lwt.t Effect.t
  | Inner : int -> unit Lwt.t Effect.t

let rec pause_n n =
  traceln (Printf.sprintf "Pausing %d times" n);
  let open Lwt.Syntax in
  if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n (n - 1)

let outer_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Outer n -> Some (fun k ->
        traceln "Handling Outer effect";
        (* Even though this code is executed outside of the stack that includes
           the frame for [Lwt_main.run], it still fails. Note that this code is
           still "executed as part of the evaluation of a callback triggered by
           a promise that is resolved by an invocation of [Lwt_main.run]". As
           such, it fails in accordance with the documentation of
           [Lwt_main.run]. *)
        Lwt_main.run (pause_n n);
        Effect.Deep.continue k (pause_n n)
    )
    | _ -> None

let inner_eff
  : type b. b Effect.t -> ((b, unit Lwt.t) Effect.Deep.continuation -> unit Lwt.t) option
  = function
    | Inner n -> Some (fun k ->
        traceln "Handling Inner effect";
        (* This also fails. See Outer handler above for details. *)
        Lwt_main.run (pause_n n);
        Effect.Deep.continue k Lwt.return_unit
    )
    | _ -> None

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        Effect.Deep.try_with
          (fun () ->
            traceln "Entering Inner handler";
            traceln "Perfoming Outer(2) effect";
            let* () = Effect.perform (Outer 2) in
            traceln "Perfomed Outer(2) effect";
            traceln "Perfoming Inner(2) effect";
            let* () = Effect.perform (Inner 2) in
            traceln "Perfomed Inner(2) effect";
            traceln "Exiting Inner handler";
            Lwt.return ()
          ) ()
          { effc = inner_eff }
      );
      traceln "Exited Inner handler";
      traceln "Exiting Outer handler";
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"


