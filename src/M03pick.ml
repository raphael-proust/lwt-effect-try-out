(** In this try-out we pick the first resolution out of two promises which are
    triggered by effects. *)

let traceln s = print_endline s

type _ Effect.t +=
  | Outer : int -> bool Lwt.t Effect.t
  | Inner : int -> bool Lwt.t Effect.t

let rec pause_n src n =
  traceln (Printf.sprintf "Pausing(%s) %d times" src n);
  let open Lwt.Syntax in
  if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n src (n - 1)

let outer_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Outer n -> Some (fun k ->
        traceln "Handling Outer effect";
        let p =
          let open Lwt.Syntax in
          let* () = pause_n "out" n in
          Lwt.return_true
        in
        Lwt.on_cancel p (fun () -> traceln "Canceled Outer");
        Effect.Deep.continue k p
    )
    | _ -> None

let inner_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Inner n -> Some (fun k ->
        traceln "Handling Inner effect";
        let p =
          let open Lwt.Syntax in
          let* () = pause_n "in" n in
          Lwt.return_false
        in
        Lwt.on_cancel p (fun () -> traceln "Canceled Inner");
        Effect.Deep.continue k p
    )
    | _ -> None

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        Effect.Deep.try_with
          (fun () ->
            traceln "Entering Inner handler";
            traceln "Perfoming Inner(4) effect";
            let p_inner = Effect.perform (Inner 4) in
            traceln "Perfomed Inner(4) effect";
            traceln "Perfoming Outer(2) effect";
            let p_outer = Effect.perform (Outer 2) in
            traceln "Perfomed Outer(2) effect";
            let p = Lwt.pick [ p_inner; p_outer ] in
            let* b = p in
            begin if b then
              traceln "Outer promise slept less and resolved first"
            else
              traceln "Inner promise slept less and resolved first"
            end;
            Lwt.return ()
          ) ()
          { effc = inner_eff }
      );
      traceln "Exited Inner handler";
      traceln "Exiting Outer handler";
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"
