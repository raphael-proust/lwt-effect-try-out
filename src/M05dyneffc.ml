(** In this try-out we work around the stack mangling using a dirty hack. *)

let traceln s = print_endline s

type _ Effect.t +=
  | E : unit -> int Effect.t

let k : unit Effect.Deep.effect_handler Lwt.key = Lwt.new_key ()

let dynamic
  : string -> int -> unit Effect.Deep.effect_handler
  = fun name init ->
    let counter = ref init in
    { Effect.Deep.effc =
      fun (type b) (e : b Effect.t) : (((b, 'a) Effect.Deep.continuation -> 'a) option) ->
      match e with
      | E () -> Some (fun k ->
          traceln (Printf.sprintf "Handling effect: %s" name);
          incr counter;
          Effect.Deep.continue k !counter
      )
      | _ -> None
    }

let static
  : unit Effect.Deep.effect_handler
  = { Effect.Deep.effc =
      fun (type b) (e : b Effect.t) : (((b, 'a) Effect.Deep.continuation -> 'a) option) ->
      traceln (Printf.sprintf "Handling effect: dispatching");
      match Lwt.get k with
      | None -> None
      | Some { effc } -> effc e
    }


let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        let work name init =
          traceln (Printf.sprintf "Setting up dynamic handler for %s" name);
          Lwt.with_value k (Some (dynamic name init)) (fun () ->
            let* () = Lwt.pause () in
            traceln (Printf.sprintf "Performing effect with %s" name);
            let x = Effect.perform (E ()) in
            let* () = Lwt.pause () in
            let* () = Lwt.pause () in
            traceln (Printf.sprintf "Performing effect with %s" name);
            let y = Effect.perform (E ()) in
            let* () = Lwt.pause () in
            Lwt.return (x + y)
          )
        in
        let* rs =
          Lwt.all [
            work "A" 0;
            work "B" 0;
            (let* () = Lwt.pause () in work "C" 10);
          ]
        in
        let r = (List.fold_left (+) 0 rs) in
        traceln (Printf.sprintf "Found result %d (expected (1+2 + 1+2 + 11+12 = 29)" r);
        Lwt.return_unit
      )
    ) ()
    static;
    traceln "Exited handler"
