(** In this try-out we pause inside of the inner effect handler. *)

let traceln s = print_endline s

type _ Effect.t +=
  | Outer : int -> unit Lwt.t Effect.t
  | Inner : int -> unit Lwt.t Effect.t

let rec pause_n n =
  traceln (Printf.sprintf "Pausing %d times" n);
  let open Lwt.Syntax in
  if n <= 0 then Lwt.return_unit else let* () = Lwt.pause () in pause_n (n - 1)

let outer_eff
  : type a b. b Effect.t -> ((b, a) Effect.Deep.continuation -> a) option
  = function
    | Outer n -> Some (fun k ->
        traceln "Handling Outer effect";
        (* We cannot pause here as it would mean the handler returns a promise.
           Because the outer-handler is installed outside of [Lwt_main.run] it
           cannot return a promise. *)
        Effect.Deep.continue k (pause_n n)
    )
    | _ -> None

let inner_eff
  : type b. b Effect.t -> ((b, unit Lwt.t) Effect.Deep.continuation -> unit Lwt.t) option
  = function
    | Inner n -> Some (fun k ->
        let open Lwt.Syntax in
        traceln "Handling Inner effect";
        let* () = pause_n n in
        Effect.Deep.continue k Lwt.return_unit
    )
    | _ -> None

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        Effect.Deep.try_with
          (fun () ->
            traceln "Entering Inner handler";
            (* This [perform Inner] works as expected. *)
            traceln "Perfoming Inner(2) effect";
            let* () = Effect.perform (Inner 2) in
            traceln "Perfomed Inner(2) effect";
            let* () = Lwt.pause () in
            (* This [perform Outer] works as expected. *)
            traceln "Perfoming Outer(2) effect";
            let* () = Effect.perform (Outer 2) in
            traceln "Perfomed Outer(2) effect";
            (* This [perform Inner] fails. It does so regardless of payload.
               - The issue is that Lwt destroys the stack of execution because we
               are behind effective pauses (more specifically, behind a bind on
               a pending promise). As a result, this call is made "outside" of
               the [try_with] stack.
               - The first [perform Inner] above works as expected because it is
               immediately at the entrance of [try_with]. More specifically,
               there are no yield points between the entrance of [try_with] and
               the call to [perform] meaning that the stack is intact.
               - The [perform Outer] above works as expected even though there
               is effective pausing and so the stack is mangled. Note however
               that the handler for this effect is installed before
               [Lwt_main.run] and so only the portion of the stack after the
               handler is mangled. *)
            traceln "Perfoming Inner(2) effect";
            let* () = Effect.perform (Inner 2) in
            traceln "Perfomed Inner(2) effect";
            traceln "Exiting Inner handler";
            Lwt.return ()
          ) ()
          { effc = inner_eff }
      );
      traceln "Exited Inner handler";
      traceln "Exiting Outer handler";
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"

let () =
  Effect.Deep.try_with
    (fun () ->
      traceln "Entering Outer handler";
      Lwt_main.run (
        let open Lwt.Syntax in
        let* () = Lwt.pause () in
        traceln "Started Lwt_main.run";
        Effect.Deep.try_with
          (fun () ->
            traceln "Entering Inner handler";
            let* () = Lwt.pause () in
            (* This [perform Inner] fails.
               This is more to-the-point version of the test above showing that
               only a single pause is necessary to mangle the stack in a way
               that causes the handler to be ineffective. *)
            traceln "Perfoming Inner(0) effect";
            let* () = Effect.perform (Inner 0) in
            traceln "Perfomed Inner(0) effect";
            Lwt.return ()
          ) ()
          { effc = inner_eff }
      );
      traceln "Exited Inner handler";
      traceln "Exiting Outer handler";
    ) ()
    { effc = outer_eff };
    traceln "Exited Outer handler"

(** As shown in this try-out: you cannot safely use [try_with] within
    [Lwt_main.run] unless you do not have any cooperation points.

{[ (* This is safe *)
Lwt_main.run
  (..
   let foo = Effect.Deep.try_with (fun () -> …) () {effc} in
   ..
  )
]}

{[ (* This is not safe in general *)
Lwt_main.run
  (..
   let* foo = Effect.Deep.try_with (fun () -> …) () {effc} in
   ..
  )
]}
*)
